[![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
<div align='center'>

<img src="https://git.science.uu.nl/GraphPolaris/frontend/-/raw/develop/src/presentation/view/navbar/logogp.png" align="center" width="150" alt="Project icon">
</div>


## Machine Learning Service
This package is used to run machine learning algorithms on query results.

### Running the service
To run this service, a new deployment must be made for each algorithm, which can then be accessed using RabbitMQ. The queuename for the consumer is stored as a variable.

### Dependencies
- Pika (RabbitMQ)
- Networkx

### Adding a new algorithm
To add a new machine learning algorithm a new folder must be added. This file must contain a wrapper.py, Dockerfile, deployment.yml and finally a python file.
The python file must implement the MLServerInterface and every function and variable it contains.

### Debugging
To debug the program the user must have a way to send RabbitMQ messages. Run the wrapper.py for the desired algorithm and it will start listening to and handling messages.