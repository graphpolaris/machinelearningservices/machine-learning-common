##
# This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
# © Copyright Utrecht University (Department of Information and Computing Sciences)
##

#!/usr/bin/env python
import networkx as nx
import json


##
# Define the interface that each algorithm should implement
##
class MLServerInterface:
    def __init__(self, service_name: str, queue_name: str):
        self.parameters = {}
        self.this = {}
        self.this[queue_name] = self.parameters
        self.service_name = service_name
        self.queue_name = queue_name

        # parameters = {}
        # this = self.MLClass(parameters, "placeholder")

    class Attribute:
        def __init__(self, optional, type):
            self.optional = optional
            self.type = type

        def toJSON(self):
            return json.dumps(
                self, default=lambda o: o.__dict__, sort_keys=True, indent=4
            )

    def __call__(self, body) -> str:
        pass
