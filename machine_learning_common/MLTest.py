##
# This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
# © Copyright Utrecht University (Department of Information and Computing Sciences)
##

#!/usr/bin/env python
import pika
import uuid
import pytest
from ShortestPath.shortestpath import MLServer

def getInput(source, target):
    return """{
        \"edges\": 
    	[
            {
                \"attributes\": {},
                \"from\": \"parliament/0\",
                \"id\": \"part_of/0\",
                \"to\": \"commissions/0\"
            },
    		{
                \"attributes\": {},
                \"from\": \"parliament/1\",
                \"id\": \"part_of/1\",
                \"to\": \"commissions/0\"
            },
    		{
                \"attributes\": {},
                \"from\": \"parliament/1\",
                \"id\": \"part_of/2\",
                \"to\": \"commissions/1\"
            },
    		{
                \"attributes\": {},
                \"from\": \"parliament/2\",
                \"id\": \"part_of/3\",
                \"to\": \"commissions/1\"
            },
    		{
                \"attributes\": {},
                \"from\": \"parliament/3\",
                \"id\": \"part_of/4\",
                \"to\": \"commissions/0\"
            }
        ],
        \"nodes\": 
    	[
    		{
                \"attributes\": 
    			{
                    \"age\": 56,
                    \"img\": \"https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/45ef5aae-a6d0-4bad-861e-dd5b883a9665.jpg?itok=h5aEZJVM\",
                    \"name\": \"Jorien Wuite\",
                    \"party\": \"D66\",
                    \"residence\": \"Voorburg\",
                    \"seniority\": 57
                },
                \"id\": \"parliament/1\",
                \"key\": \"147\",
                \"rev\": \"_c78I9v---j\"
            },
    		{
                \"attributes\": 
    			{
                    \"age\": 56,
                    \"img\": \"https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/45ef5aae-a6d0-4bad-861e-dd5b883a9665.jpg?itok=h5aEZJVM\",
                    \"name\": \"Jorien Wuite\",
                    \"party\": \"D66\",
                    \"residence\": \"Voorburg\",
                    \"seniority\": 57
                },
                \"id\": \"parliament/2\",
                \"key\": \"147\",
                \"rev\": \"_c78I9v---j\"
            },
    		{
                \"attributes\": 
    			{
                    \"age\": 31,
                    \"img\": \"https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/397c857a-fda0-414d-8fdc-8288cd3284aa.jpg?itok=55l5zRvr\",
                    \"name\": \"Thierry Aartsen\",
                    \"party\": \"VVD\",
                    \"residence\": \"Breda\",
                    \"seniority\": 987
                },
                \"id\": \"parliament/0\",
                \"key\": \"0\",
                \"rev\": \"_c78I9u2---\"
    		},
    		{
                \"attributes\": 
    			{
                    \"age\": 31,
                    \"img\": \"https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/397c857a-fda0-414d-8fdc-8288cd3284aa.jpg?itok=55l5zRvr\",
                    \"name\": \"Thierry Aartsen\",
                    \"party\": \"VVD\",
                    \"residence\": \"Breda\",
                    \"seniority\": 987
                },
                \"id\": \"parliament/3\",
                \"key\": \"0\",
                \"rev\": \"_c78I9u2---\"
    		},
    		{
                \"attributes\": {
                    \"name\": \"Commissie voor de Inlichtingen- en Veiligheidsdiensten\"
                },
                \"id\": \"commissions/0\",
                \"key\": \"36\",
                \"rev\": \"_c78rkVi--F\"
            },
    		{
                \"attributes\": {
                    \"name\": \"Commissie voor de Inlichtingen- en Veiligheidsdiensten\"
                },
                \"id\": \"commissions/1\",
                \"key\": \"36\",
                \"rev\": \"_c78rkVi--F\"
            }
        ],
    	\"machineLearning\":
        [
            {
                \"queueName\":\"stp_queue\"
                \"params\":[\"""" + source + """\",\"""" + target + """\"]
            }
        ]
    }"""

def test_1():
    server = MLServer()
    result = server.handleJSON(getInput("parliament/0", "parliament/1").encode())
    assert result == output1


source = "parliament/0"
target = "parliament/1"



output1 = """{
    \"edges\": [
        {
            \"attributes\": {},
            \"from\": \"parliament/0\",
            \"id\": \"part_of/0\",
            \"to\": \"commissions/0\"
        },
        {
            \"attributes\": {},
            \"from\": \"parliament/1\",
            \"id\": \"part_of/1\",
            \"to\": \"commissions/0\"
        },
        {
            \"attributes\": {},
            \"from\": \"parliament/1\",
            \"id\": \"part_of/2\",
            \"to\": \"commissions/1\"
        },
        {
            \"attributes\": {},
            \"from\": \"parliament/2\",
            \"id\": \"part_of/3\",
            \"to\": \"commissions/1\"
        },
        {
            \"attributes\": {},
            \"from\": \"parliament/3\",
            \"id\": \"part_of/4\",
            \"to\": \"commissions/0\"
        }
    ],
    \"nodes\": [
        {
            \"attributes\": {
                \"age\": 56,
                \"img\": \"https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/45ef5aae-a6d0-4bad-861e-dd5b883a9665.jpg?itok=h5aEZJVM\",
                \"name\": \"Jorien Wuite\",
                \"party\": \"D66\",
                \"residence\": \"Voorburg\",
                \"seniority\": 57
            },
            \"id\": \"parliament/1\",
            \"key\": \"147\",
            \"rev\": \"_c78I9v---j\"
        },
        {
            \"attributes\": {
                \"age\": 56,
                \"img\": \"https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/45ef5aae-a6d0-4bad-861e-dd5b883a9665.jpg?itok=h5aEZJVM\",
                \"name\": \"Jorien Wuite\",
                \"party\": \"D66\",
                \"residence\": \"Voorburg\",
                \"seniority\": 57
            },
            \"id\": \"parliament/2\",
            \"key\": \"147\",
            \"rev\": \"_c78I9v---j\"
        },
        {
            \"attributes\": {
                \"age\": 31,
                \"img\": \"https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/397c857a-fda0-414d-8fdc-8288cd3284aa.jpg?itok=55l5zRvr\",
                \"name\": \"Thierry Aartsen\",
                \"party\": \"VVD\",
                \"residence\": \"Breda\",
                \"seniority\": 987
            },
            \"id\": \"parliament/0\",
            \"key\": \"0\",
            \"rev\": \"_c78I9u2---\",
            \"mldata\": {
                \"parliament/1\": [
                    \"parliament/0\",
                    \"commissions/0\",
                    \"parliament/1\"
                ]
            }
        },
        {
            \"attributes\": {
                \"age\": 31,
                \"img\": \"https://www.tweedekamer.nl/sites/default/files/styles/member_parlement_profile_square/public/397c857a-fda0-414d-8fdc-8288cd3284aa.jpg?itok=55l5zRvr\",
                \"name\": \"Thierry Aartsen\",
                \"party\": \"VVD\",
                \"residence\": \"Breda\",
                \"seniority\": 987
            },
            \"id\": \"parliament/3\",
            \"key\": \"0\",
            \"rev\": \"_c78I9u2---\"
        },
        {
            \"attributes\": {
                \"name\": \"Commissie voor de Inlichtingen- en Veiligheidsdiensten\"
            },
            \"id\": \"commissions/0\",
            \"key\": \"36\",
            \"rev\": \"_c78rkVi--F\"
        },
        {
            \"attributes\": {
                \"name\": \"Commissie voor de Inlichtingen- en Veiligheidsdiensten\"
            },
            \"id\": \"commissions/1\",
            \"key\": \"36\",
            \"rev\": \"_c78rkVi--F\"
        }
    ]
}"""