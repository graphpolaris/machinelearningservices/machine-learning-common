##
# This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
# © Copyright Utrecht University (Department of Information and Computing Sciences)
##

#!/usr/bin/env python
import logging
import traceback
from machine_learning_common.MLRepositoryInterface import MLServerInterface
import pika
import json
import redis
import os


##
# void does nothing and is later overwritten by a different function in order to pass other functions as parameters
#   DikkeDraak: Any, dummy parameter
#   Return: None, returns nothing
##
def void(DikkeDraak):
    pass


# We often compare against this specific string, so we clearly define it to prevent any typos
# Python has no constants so we simply give it a very obvious name that implies it is not supposed to be changed
CONST_MLEXCHANGE = "ml-direct-exchange"
CONST_UIEXCHANGE = "ui-direct-exchange"
# Set out dummy function to a variable so we can access it everywhere where we need it
func = void
rabbit_username = os.getenv("RABBIT_USER")
rabbit_password = os.getenv("RABBIT_PASSWORD")
rabbit_host = os.getenv("RABBIT_HOST", default="rabbitmq")
rabbit_port = os.getenv("RABBIT_PORT", default=5672)
credentials = pika.PlainCredentials(rabbit_username, rabbit_password)
# credentials = pika.PlainCredentials('EjXKOP20J4sB_jgq9FX9tJ2X4eaI04OU', 'pswmQPeyuXm0teQAiu0q-z-DmopH_vgu')

# Set up Redis
# THESE PARAMETERS ARE DUMMY PARAMS, WE SHOULD CONNECT TO THE REDIS MICROSERVICE PARAMS
redis_address, redis_port = os.getenv("REDIS_ADDRESS", default="redis:6379").split(":")
redis_password = os.getenv("REDIS_PASSWORD")
print("redis address:", redis_address, redis_port)
print("rabbit address:", rabbit_host, rabbit_port)
r = redis.Redis(
    host=redis_address,
    password=redis_password,
    port=redis_port,
    db=0,
    decode_responses=True,
)


class MLDriver:
    def __init__(self, service: MLServerInterface):
        self.service = service

    ##
    # on_request is a callback function that runs every time a request is consumed from the message queue
    #   ch: Any, the RabbitMQ channel
    #   method: Any, the function that should be called within the body of this function
    #   props: Any, the RabbitMQ information that is not contained in the body
    #   body: Any, the body of the RabbitMQ message
    #   Return: None, returns nothing
    ##
    def on_request(self, ch, method, props, body):
        message = json.loads(props.headers["message"].decode())
        print("message is: ", message)

        sessionID = message["sessionData"]["sessionID"]
        userID = message["sessionData"]["userID"]

        print("SessionID is: " + sessionID)
        print("userID is: " + userID)
        clientData = r.get("routing " + sessionID)
        clientQueueID = json.loads(clientData)["QueueID"]
        logging.info(
            f"Incoming request with ClientQueueID: {clientQueueID}, sessionID: {sessionID}"
        )

        testing = os.getenv("INTEGRATION_TESTING")
        if not (isinstance(clientQueueID, str)):
            if testing == "true":
                clientQueueID = "test-queue"

        if not (isinstance(clientQueueID, str)):
            print("There is no value for that key, stop asking")
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return
        print("ClientQueueID is: " + clientQueueID)
        try:
            result = self.service(body)
            ret = dict(type=self.service.service_name, value=result)
            # The sessionID is technically in the header, but I think rabbitMQ treats it as a single message (the body)
            print("Result created", len(ret), ret["type"])
            self.send_message(ch, clientQueueID, props, ret)
            # This is the queue ID taken from our Redis, these are bytes to we have to convert to strings first
            ch.basic_ack(delivery_tag=method.delivery_tag)
            print("Results sent")

        except Exception as e:
            print("generic ml fail: ", e)
            traceback.print_exc()
            try:
                self.send_error(
                    ch, clientQueueID, body, props, "ML bad request: " + e.message
                )
            except:
                self.send_error(
                    ch, clientQueueID, body, props, "ML bad request: " + str(e)
                )

    ##
    # listen listens and consumes messages from a queue
    #   queuename: Any, the RabbitMQ queuename we're listening to
    #   callbackfunc: Any, the function that should be used on callback, it overrides our func var
    #   Return: None, returns nothing
    ##
    def listen(self):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=rabbit_host, port=rabbit_port, credentials=credentials
            )
        )

        channel = connection.channel()
        # Declare the ML exchange to which we listen
        channel.exchange_declare(
            exchange=CONST_MLEXCHANGE,
            exchange_type="direct",
            durable=True,
            auto_delete=False,
            internal=False,
            arguments=None,
        )

        channel.queue_declare(queue=self.service.queue_name)

        channel.basic_qos(prefetch_count=1)

        channel.queue_bind(exchange=CONST_MLEXCHANGE, queue=self.service.queue_name)

        channel.basic_consume(
            queue=self.service.queue_name, on_message_callback=self.on_request
        )

        print(" [x] Awaiting RPC requests on " + self.service.queue_name)
        channel.start_consuming()

    # Sends an error message to the frontend
    def send_error(
        self, ch: pika.BlockingConnection, clientQueueID, body, props, reason
    ):
        # Decode body in order to get queryID
        data = json.loads(body.decode())
        # Construct new message body
        error_data = {
            "type": "ml_error",
            "value": {
                "status": reason,
                "queryID": data["queryID"],
            },
        }
        self.send_message(ch, clientQueueID, props, error_data)
        print("Error message sent")

    def send_message(self, ch: pika.BlockingConnection, clientQueueID, props, body):
        ch.exchange_declare(
            exchange=CONST_UIEXCHANGE,
            exchange_type="direct",
            durable=True,
            auto_delete=False,
            internal=False,
            arguments=None,
        )

        ch.basic_publish(
            exchange=CONST_UIEXCHANGE,
            routing_key=clientQueueID,
            properties=props,
            body=json.dumps(body),
        )
